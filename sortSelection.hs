import Data.List.Split
--swapSelecte: swap element sort for less item
swapSelection :: Ord a => [a] -> [a]
swapSelection [x1] = [x1]
swapSelection (x1:x2:x) 
    | x1>x2 = r:x1:rs
    | otherwise = r:x2:rs
	where
    (r:rs) | x1>x2 = swapSelection (x2:x)
           | otherwise = swapSelection (x1:x)
----sortSelecte: sort array    
sortSelection :: Ord a => [a] -> [a]
sortSelection [] = []
sortSelection x = y1 : sortSelection ys
	where
	(y1:ys) = swapSelection x
--main get array
main :: IO ()
main = do
    putStrLn "Hello, sort selection [3,2,1]"
    n <- getLine
    putStrLn (show (sortSelection ((read n)::[Int])))